(function() {

    "use strict";
    const navbarMenu = document.querySelector('.navbar-menu');
    const toggles = document.querySelectorAll(".c-hamburger");

    for (let i = toggles.length - 1; i >= 0; i--) {
        let toggle = toggles[i];
        toggleHandler(toggle);
    }

    function toggleHandler(toggle) {
        toggle.addEventListener( "click", function(e) {
            e.preventDefault();
            if(this.classList.contains("is-active") === true){ this.classList.remove("is-active");
                navbarMenu.style.display = 'none'
            }
            else{
            this.classList.add("is-active");
                navbarMenu.style.display = 'inline-block'
            }
        });
    }

})();
