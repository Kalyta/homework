import React from "react";
import Modal from "./containers/modal/modal";
import ActionButton from "./components/button/action-button";
import "./app.scss";

class App extends React.Component {
  state = {
    firstFired: false,
    secondFired: false,
    isOpened: false
  };

  openFirstModal = () => {
    this.setState(prevState => ({
      firstFired: !prevState.firstFired,
      isOpened: true
    }));
  };

  openSecondModal = () => {
    this.setState(prevState => ({
      secondFired: !prevState.secondFired,
      isOpened: true
    }));
  };

  closeModal = event => {
    let target = event.target;
    console.log(target.tagName);
    if (target.tagName !== "DIV") {
      this.setState(state => {
        return {
          isOpened: false
        };
      });
    }
  };

  render() {
    const { firstFired, secondFired, isOpened } = this.state;
    console.log(this.state);
    return (
      <>
        <div className="buttons-container">
          <ActionButton
            className="red opener-btn"
            action={this.openFirstModal}
            text="Open first modal"
          />
          <ActionButton
            className="blue opener-btn"
            action={this.openSecondModal}
            text="Open second modal"
          />
        </div>
        {firstFired && isOpened ? (
          <Modal
            text="If you change 'Yes', your money will be deducted from your card"
            className="first-modal"
            action={this.closeModal}
            header="Do you want to buy a new car?"
          />
        ) : null}
        {secondFired && isOpened ? (
          <Modal
            text="If you change 'Yes', your money will be deducted from your card"
            className="second-modal"
            action={this.closeModal}
            header="Would you like to drink one cup of coffee?"
          />
        ) : null}
      </>
    );
  }
}

export default App;
