const SHOP_DATA = [
    {
        id: 1,
        name: "White T-shirt",
        imageUrl: "https://i.ibb.co/CmrtYN4/20200124143940-005324035-3.jpg",
        price: 30
    },
    {
        id: 2,
        name: "Black T-shirt",
        imageUrl: "https://i.ibb.co/rGYCkDH/20200219121729-005347526-3.jpg",
        price: 32
    },
    {
        id: 3,
        name: "Leather Bag",
        imageUrl: "https://i.ibb.co/YLxWMGJ/20140519111753-001049136-4.jpg",
        price: 35
    },
    {
        id: 4,
        name: "Leather Wallet",
        imageUrl: "https://i.ibb.co/344hjFF/20191104121333-005254811-2.jpg",
        price: 20
    },
    {
        id: 5,
        name: "White Shoes",
        imageUrl: "https://i.ibb.co/TmvsNmz/20200111093806-005310191-1.jpg",
        price: 50
    },
    {
        id: 6,
        name: "Brown Shoes",
        imageUrl: "https://i.ibb.co/G3Xy4BP/20200111093808-005310323-1.jpg",
        price: 60
    },
    {
        id: 7,
        name: "Sandals",
        imageUrl: "https://i.ibb.co/Jtm37dW/20190725141818-005150314-5.jpg",
        price: 45
    },
    {
        id: 8,
        name: "Grey Sweatshirt",
        imageUrl: "https://i.ibb.co/SQSrLHb/20200122115131-005319701-1.jpg",
        price: 35
    },
    {
        id: 9,
        name: "Red Sweatshirt",
        imageUrl: "https://i.ibb.co/VQ3dHWg/20190202093328-004876354-1.jpg",
        price: 35
    },
    {
        id: 10,
        name: "Blue Windbreaker",
        imageUrl: "https://i.ibb.co/pzgwzMc/20200111093808-005310298-1.jpg",
        price: 80
    },
    {
        id: 11,
        name: "Blue Down-jacket",
        imageUrl: "https://i.ibb.co/RGFmxSR/20181228145847-004770523-3.jpg",
        price: 140
    },
    {
        id: 12,
        name: "Black Down-jacket",
        imageUrl: "https://i.ibb.co/rpWQP5n/20191121151848-005274688-3.jpg",
        price: 120
    }
];

export default SHOP_DATA;