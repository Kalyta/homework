import React from 'react';
import './App.css';
import CartContainer from "./containers/cartcontainer/cartcontainer"
import "bootstrap/dist/css/bootstrap.min.css";

const test = [{ 1: "test" }];
localStorage.setItem("Cards", JSON.stringify(test));

function App() {
  return (
    <div className ="App">
        <CartContainer/>
    </div>
  );
}

export default App;
