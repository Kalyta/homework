class Board {
    constructor(){}

    render() {

        const createContainer = document.createElement('div');
        createContainer.classList.add('container');
        document.querySelector("script").before(createContainer);

        function allowDrop(e) {
            e.preventDefault()
        }

        function drop(e) {
            e.preventDefault();
            const data = e.dataTransfer.getData("text");
            this.append(document.getElementById(data));
            e.dataTransfer.clearData()
        }

        const table = document.getElementsByClassName("container");
        for (let i = 0; i < table.length; i++) {
            table[i].addEventListener("dragover", allowDrop);
            // table[i].addEventListener("drop", drop);
        }

        const buttonCardConstructor = document.createElement('button');
        buttonCardConstructor.classList.add('buttonCardConstructor');
        buttonCardConstructor.setAttribute("id", "openModal");
        buttonCardConstructor.innerText = "Create card";
        createContainer.append(buttonCardConstructor);

        buttonCardConstructor.addEventListener("click", () => {
            const modalContainer = document.createElement("div");
            modalContainer.classList.add("modal");
            createContainer.before(modalContainer);

            const modalContent = document.createElement("div");
            modalContent.classList.add("modal-content");
            modalContainer.append(modalContent);

            const closeModal = document.createElement("span");
            closeModal.classList.add("modal-close");
            closeModal.innerHTML = "&times;";
            modalContent.append(closeModal);

            const modalInput = document.createElement("input");
            modalInput.classList.add("cardText");
            modalContent.append(modalInput);

            const confirmButton = document.createElement("button");
            confirmButton.classList.add("confirmButton");
            confirmButton.innerText = "OK";
            modalContent.append(confirmButton);


            let modalInputValue = undefined;
            modalInput.addEventListener("keyup", (e) => {
                modalInputValue  = e.target.value;
                return modalInputValue
            });

            confirmButton.addEventListener('click', ()=>{
                const card = document.createElement("div");

                const id = (function() {
                    return (
                        "card-id-" +
                        Math.random()
                            .toString(36)
                            .substr(2, 3)
                    )
                })();

                card.setAttribute("id", `${id}`);
                card.setAttribute("draggable", "true");
                card.classList.add("card");
                modalContainer.style.display = "none";
                createContainer.append(card);

                function drag(e) {
                    e.dataTransfer.setData("text", modalInputValue);
                }

                const cardText = document.createElement("p");
                cardText.innerText = modalInputValue;
                card.append(cardText);

                const cards = document.querySelectorAll(".card");
                cards.forEach(item => {
                    item.addEventListener("dragstart", drag);
                    item.addEventListener("drop", drop)
                });

            });

            closeModal.addEventListener("click", e => {
                modalContainer.style.display = "none"
            });




        })
    }
}

const result = new Board();
result.render();