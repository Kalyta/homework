/**
 * Класс, объекты которого описывают параметры гамбургера.
 *
 * @constructor
 * @param size        Размер
 * @param stuffing    Начинка
 * @throws {HamburgerException}  При неправильном использовании
 */
function Hamburger(size, stuffing){

    if(!size){
        throw new HamburgerException('no size given');
    }
    if (!stuffing){
        throw new HamburgerException('no stuffing given');
    }
    if (size.type !== "size"){
        throw new HamburgerException('you put not a size');
    }
    if (stuffing.type !== "stuffing"){
        throw new HamburgerException('you put not a stuffing');
    }

        this.size = size;
        this.stuffing = stuffing;
        this.toppings = [];

}
/* Размеры, виды начинок и добавок */
Hamburger.SIZE_SMALL = {price: 50, calories: 20, type: "size"};
Hamburger.SIZE_LARGE = {price: 100, calories: 40, type: "size"};
Hamburger.STUFFING_CHEESE = {price: 10, calories: 20, type: "stuffing", name: "cheese"};
Hamburger.STUFFING_SALAD = {price: 20, calories: 5, type: "stuffing", name: "salad"};
Hamburger.STUFFING_POTATO = {price: 15, calories: 10, type: "stuffing", name: "potato"};
Hamburger.TOPPING_MAYO = {price: 20, calories: 5, type: "topping", name: "mayo"};
Hamburger.TOPPING_SPICE = {price: 15, calories: 0, type: "topping", name: "spice"};
/**
 * Добавить добавку к гамбургеру. Можно добавить несколько
 * добавок, при условии, что они разные.
 *
 * @param topping     Тип добавки
 * @throws {HamburgerException}  При неправильном использовании
 */
Hamburger.prototype.addTopping = function (topping){
    if (this.toppings.includes(topping)){
        throw new HamburgerException('you tried to duplicate topping')
    }
    if (!topping || topping.type !== 'topping') {
        throw new HamburgerException('its not a topping')
    }
    this.toppings.push(topping)
};
/**
 * Убрать добавку, при условии, что она ранее была
 * добавлена.
 *
 * @param topping   Тип добавки
 * @throws {HamburgerException}  При неправильном использовании
 */
Hamburger.prototype.removeTopping = function (topping) {
    if (!this.toppings.includes(topping)){
        throw new HamburgerException('you tried to remove unexistent topping')
    }
    this.toppings.splice(this.toppings.indexOf(topping), 1)
};
/**
 * Получить список добавок.
 *
 * @return {Array} Массив добавленных добавок, содержит константы
 *                 Hamburger.TOPPING_*
 */
Hamburger.prototype.getToppings = function (){
    return this.toppings
};
/**
 * Узнать размер гамбургера
 */
Hamburger.prototype.getSize = function (){
    return this.size
};
/**
 * Узнать начинку гамбургера
 */
Hamburger.prototype.getStuffing = function (){
    return this.stuffing
};
/**
 * Узнать цену гамбургера
 * @return {Number} Цена в тугриках
 */
Hamburger.prototype.calculatePrice = function (){
let price = this.size.price + this.stuffing.price;
this.toppings.forEach(topping => {
    price += topping.price
});
return price;
};
/**
 * Узнать калорийность
 * @return {Number} Калорийность в калориях
 */
Hamburger.prototype.calculateCalories = function (){
    let calories = this.size.calories + this.stuffing.calories;
    this.toppings.forEach(topping => {
        calories += topping.calories
    });
    return calories;
};
/**
 * Представляет информацию об ошибке в ходе работы с гамбургером.
 * Подробности хранятся в свойстве message.
 * @constructor
 */
function HamburgerException (massage) {
    this.name = "HamburgerException";
    this.massage = massage;
}
let hamburger;

try{
    hamburger = new Hamburger(Hamburger.SIZE_LARGE, Hamburger.STUFFING_POTATO);
} catch (e) {
    console.log(e)
}
try{
    hamburger.addTopping(Hamburger.STUFFING_SALAD);
} catch (e) {
    console.log(e)
}
try{
    hamburger.removeTopping(Hamburger.TOPPING_SPICE);
} catch (e) {
    console.log(e)
}

console.log(hamburger);

console.log(hamburger.calculateCalories());



