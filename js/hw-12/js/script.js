let images = document.querySelectorAll('.image-to-show');
images[0].classList.add('active');
let timerID = null;

function startSlideShow() {
    let intervalID = setInterval(() => {
        let activeImgIndex = +document.querySelector('.image-to-show.active').dataset.index;
        let nextImgIndex = (activeImgIndex === images.length) ? 1 : activeImgIndex + 1 ;


        for (let img of images) {
            img.classList.remove('active');
        }
        for (let img of images) {
            if (+img.dataset.index === nextImgIndex) {
                img.classList.add('active');
            }
        }
    }, 2000);

    return intervalID;
}

timerID = startSlideShow();

let buttonFirst = document.getElementById('stop-show');
buttonFirst.addEventListener('click', () => {
    clearInterval(timerID)
});
let buttonSecond = document.getElementById('start-show');
buttonSecond.addEventListener('click', () => {
    timerID = startSlideShow();
});