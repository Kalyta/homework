let array = ['hello', 'world', 'Kiev', 'Kharkiv', 'Odessa', 'Lviv'];

function arrayOutput(array) {
    let list = document.createElement('ul');

    const arrayListItems = array.map(item => {
        let listItem = document.createElement('li');
        listItem.innerText = item;
        return listItem;
    });

    arrayListItems.forEach(li => {
        list.appendChild(li)
    });
    return list
}

document.querySelector('script').before(arrayOutput(array));