let uncorrectValue = document.getElementById('uncorrectValue');
const inputFirst = document.getElementById('input-1');
const inputSecond = document.getElementById('input-2');
const visibility = document.getElementsByClassName('fas');
const labelBlocks = document.getElementsByClassName('input-wrapper');

function changePasswordVisible(lable) {
    switch (lable.children[0].type) {
        case "password": {
            lable.children[0].type = "text";
            lable.children[1].classList.remove('fa-eye');
            lable.children[1].classList.add('fa-eye-slash');
            break;
        }
        case "text": {
            lable.children[0].type = "password";
            lable.children[1].classList.remove('fa-eye-slash');
            lable.children[1].classList.add('fa-eye');
            break;
        }
        default: {
            console.log('Something went wrong.... ');
        }
    }
}
const button = document.getElementById('button');
    button.addEventListener('click', () => {
        uncorrectValue.innerText = '';
    if(inputFirst.value === inputSecond.value){
        setTimeout(() =>alert('You are welcome!'), 0)
    }else {
        uncorrectValue.innerText = 'Нужно ввести одинаковые значения';
        uncorrectValue.style.display = 'block';
        uncorrectValue.style.color = 'red'
    }
});
visibility[0].onclick = () => changePasswordVisible(labelBlocks[0]);
visibility[1].onclick = () => changePasswordVisible(labelBlocks[1]);