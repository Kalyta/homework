
const blackThemePath = 'css/style.css';
const redThemePath = 'css/redstyle.css';

if (!localStorage.getItem('theme')) {
    localStorage.setItem('theme', blackThemePath);
} else {
    const themeLink = document.getElementById('page-theme');
    const themePath = localStorage.getItem('theme');
    themeLink.setAttribute('href', themePath);
}


const changeThemeBtn = document.getElementById('change-color');
changeThemeBtn.addEventListener('click', toggleTheme);

function toggleTheme() {
    const themeLink = document.getElementById('page-theme');
    const currentThemePath = themeLink.getAttribute('href');

    if (currentThemePath === blackThemePath) {
      localStorage.setItem('theme', redThemePath);
      themeLink.setAttribute('href', redThemePath);
  } else {
      localStorage.setItem('theme', blackThemePath);
      themeLink.setAttribute('href', blackThemePath);
  }
}

