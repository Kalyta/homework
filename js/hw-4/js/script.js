
function createNewUser() {
    let firstName = prompt('Enter your Firstname, please', 'Name');
    let lastName = prompt('Enter your Lastname, please', 'Name');

    let newUser = {
        firstName: firstName,
        lastName: lastName,

        getLogin: function () {
            return (this.firstName[0] + this.lastName).toLowerCase();
        },
        setFirstName: function (name) {
            Object.defineProperty(newUser, 'firstName', {
                value: name
            })
        },
        setLastName: function (lastName) {
            Object.defineProperty(newUser, 'lastName', {
                value: lastName
            })
        }
    };
    Object.defineProperty(newUser, 'firstName', {
        writable: false,
        value: firstName
    });
    Object.defineProperty(newUser, 'lastName', {
        writable: false,
        value: lastName
    });
    alert(newUser.getLogin());
    return newUser;
}
let user = createNewUser();
console.log(user);

user.firstName = '123456';
user.lastName = '123456';
user.setFirstName('namer');
user.setLastName('lastNamer');
console.log(user);