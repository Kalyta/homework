


    $( document ).ready(function(){
        //плавная прокрутка по ссылкам

        $('a[href^="#"]').click(function () {

            let target = $(this).attr('href');
            console.log(target);
            $("html, body").animate({scrollTop: $(target).offset().top}, 1000);
            return false;
        });

        // slide toggle

        $( ".slide-toggle" ).click(function(){
            $( ".most-popular-posts" ).slideToggle('slow');
        });

        // прокрутка вверх
        const $scrolTopBtn = $('.scrol-top-btn')

        $scrolTopBtn.click(() => {
            $('body, html').animate({
                scrollTop: 0
            }, 1000);
        });

        // отображение кнопки

        $(document).scroll(function (e) {
            const $screenHeight = $(window).innerHeight();
            const $screenTop = $(window).scrollTop();
            if ($screenTop > $screenHeight) {
                $scrolTopBtn.fadeIn(500);
            } else {
                $('.scrol-top-btn').fadeOut(500)
            }
        });

    });



